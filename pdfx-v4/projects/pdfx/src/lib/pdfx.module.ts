import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
// import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';
import { NgCoreMarkModule } from '@ngcore/mark';
import { NgCoreIdleModule } from '@ngcore/idle';

import { CommonPDFxService } from './services/common-pdfx-service';
import { CommonPdfEntryComponent } from './components/common-pdf-entry/common-pdf-entry';
import { CommonPDFxEntryComponent } from './components/common-pdfx-entry/common-pdfx-entry';
import { CommonPdfButtonComponent } from './components/common-pdf-button/common-pdf-button';
import { CommonPDFxButtonComponent } from './components/common-pdfx-button/common-pdfx-button';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    // BrowserModule,
    // IonicModule,

    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
    NgCoreMarkModule.forRoot(),
    NgCoreIdleModule.forRoot()
  ],
  declarations: [
    CommonPdfEntryComponent,
    CommonPDFxEntryComponent,
    CommonPdfButtonComponent,
    CommonPDFxButtonComponent
  ],
  exports: [
    CommonPdfEntryComponent,
    CommonPDFxEntryComponent,
    CommonPdfButtonComponent,
    CommonPDFxButtonComponent
  ],
  entryComponents: [
    CommonPdfEntryComponent,
    CommonPDFxEntryComponent,
    // ????
  ]
})
export class NgCorePDFxModule {
  static forRoot(): ModuleWithProviders<NgCorePDFxModule> {
    return {
      ngModule: NgCorePDFxModule,
      providers: [
        // { provide: ErrorHandler, useClass: IonicErrorHandler },
        CommonPDFxService
      ]
    };
  }
}
