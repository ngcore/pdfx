import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
// import { NavController, AlertController, ToastController, Events } from 'ionic-angular';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';


// tbd
// Placeholder, for now...


@Component({
  selector: 'common-pdf-entry',
  template: `
  <div [innerHTML]="pdfText">
  </div>
`
})
export class CommonPdfEntryComponent {

  pdfText: string;

  // @Input("iid") itemId: string;
  @Input("pdf") pdfInput: string;

  constructor(
    // private navCtrl: NavController,
    // private alertCtrl: AlertController,
    // private toastCtrl: ToastController,
    // private events: Events
  ) {
    // if(isDL()) dl.log('Hello CommonPdfEntry Component. id = ' + this.navCtrl.id);

    this.pdfText = "";
  }
  ngOnInit() {
    // // variables can be read here....
    // // if(isDL()) dl.log(">>>>>> this.itemId = " + this.itemId);
    // if(isDL()) dl.log(">>>>>> this.pdfInput = " + this.pdfInput);

    // TBD:
    if (this.pdfInput) {
      this.pdfText = this.pdfInput;
    }
    // ....
  }

  setPdfInput(_pdfInput: string) {
    this.pdfInput = _pdfInput;
    this.pdfText = this.pdfInput;
  }

}
