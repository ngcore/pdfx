import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
// import { NavController, AlertController, ToastController, Events } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
// import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { CommonPDFxUtil } from '../../common/util/common-pdfx-util';


// tbd
// Placeholder, for now...


@Component({
  selector: 'common-pdfx-button',
  template: `
  <button (click)="openPDFxWindow($event)">
  </button>
`
})
export class CommonPDFxButtonComponent {

  pdfxText: string;

  // @Input("iid") itemId: string;
  @Input("markdown") markdownInput: string;

  constructor(
    private httpClient: HttpClient,
    // private navCtrl: NavController,
    // private alertCtrl: AlertController,
    // private toastCtrl: ToastController,
    // private events: Events
  ) {
    // if(isDL()) dl.log('Hello CommonPDFxButton Component. id = ' + this.navCtrl.id);

    // testing
    // this.pdfxText = "<em>Hello</em>";
    this.pdfxText = "";
  }
  ngOnInit() {
    // // variables can be read here....
    // // if(isDL()) dl.log(">>>>>> this.itemId = " + this.itemId);
    // if(isDL()) dl.log(">>>>>> this.markdownInput = " + this.markdownInput);

    // TBD:
    if (this.markdownInput) {
      let converted = CommonPDFxUtil.convertToPDF(this.markdownInput);
      this.pdfxText = (converted) ? converted : '';
    }
    // ....

  }

  setPDFxInput(_markdownInput: string, showDebugLog = false) {
    this.markdownInput = _markdownInput;
    let oldPDF = this.pdfxText;   // Last successful conversion....
    let converted = CommonPDFxUtil.convertToPDF(this.markdownInput, showDebugLog);
    if (converted == null) {
      if(isDL()) dl.log('CommonPDFxButtonComponent: setPDFxInput() Failed conversion: _markdownInput = ' + _markdownInput);
    }
    // this.pdfxText = (converted) ? converted : '';
    this.pdfxText = (converted != null) ? converted : oldPDF;   // ????
  }


  // tbd:
  openPDFxWindow(event) {
    if(isDL()) dl.log("openPDFxWindow() event = " + event);

    // tbd:
    // open window and display pdfxText

    if(isDL()) dl.log(">>>> pdfxText = ");
    if(isDL()) dl.log(this.pdfxText);
    
  }

}
