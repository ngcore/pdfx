import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
// import { NavController, AlertController, ToastController, Events } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
// import * as commonmark from 'commonmark';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { CommonPDFxUtil } from '../../common/util/common-pdfx-util';


// tbd
// Placeholder, for now...


@Component({
  selector: 'common-pdfx-entry',
  template: `
  <div [innerHTML]="pdfxText">
  </div>
`
})
export class CommonPDFxEntryComponent {

  pdfxText: string;

  // @Input("iid") itemId: string;
  @Input("markdown") markdownInput: string;

  constructor(
    private httpClient: HttpClient,
    // private navCtrl: NavController,
    // private alertCtrl: AlertController,
    // private toastCtrl: ToastController,
    // private events: Events
  ) {
    // if(isDL()) dl.log('Hello CommonPDFxEntry Component. id = ' + this.navCtrl.id);

    // testing
    // this.pdfxText = "<em>Hello</em>";
    this.pdfxText = "";
  }
  ngOnInit() {
    // // variables can be read here....
    // // if(isDL()) dl.log(">>>>>> this.itemId = " + this.itemId);
    // if(isDL()) dl.log(">>>>>> this.markdownInput = " + this.markdownInput);

    // TBD:
    if (this.markdownInput) {
      let converted = CommonPDFxUtil.convertToPDF(this.markdownInput);
      this.pdfxText = (converted) ? converted : '';
    }
    // ....

  }

  setPDFxInput(_markdownInput: string, showDebugLog = false) {
    this.markdownInput = _markdownInput;
    let oldHTML = this.pdfxText;   // Last successful conversion....
    let converted = CommonPDFxUtil.convertToPDF(this.markdownInput, showDebugLog);
    if (converted == null) {
      if(isDL()) dl.log('CommonPDFxEntryComponent: setPDFxInput() Failed conversion: _markdownInput = ' + _markdownInput);
    }
    // this.pdfxText = (converted) ? converted : '';
    this.pdfxText = (converted != null) ? converted : oldHTML;   // ????
  }


  // Moved to CommonPDFxUtil.
  // public static convertToHTML(markdownStr: string): string {
  //   if(isDL()) dl.log('convertToHTML() with markdownStr = ' + markdownStr);
  //   let reader = new commonmark.Parser();
  //   let writer = new commonmark.HtmlRenderer({safe: true});
  //   let parsed = reader.parse(markdownStr);
  //   let pdfxStr = writer.render(parsed);
  //   if(isDL()) dl.log('convertToHTML() generated pdfxStr = ' + pdfxStr);
  //   return pdfxStr;
  // }


}
