# NgCore - PDFx
> NG Core angular/typescript PDF library


PDF helper classes library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/pdfx/



